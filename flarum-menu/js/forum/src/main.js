import { extend } from 'flarum/extend';
import HeaderPrimary from 'flarum/components/HeaderPrimary';
import HeaderSecondary from 'flarum/components/HeaderSecondary'
import Post from
'flarum/components/Post';

app.initializers.add('binh-menu', function() {

	extend(HeaderPrimary.prototype, 'items', function(items) {
	 items.add('shop', <a href="t/a" className="Button Button--link">SHOP</a>);
	 items.add('product', <a href="t/b" className="Button Button--link">PRODUCT</a>); 
	 /*var current = top.location.href;
	 var url = "https://www.facebook.com/dialog/share?app_id=1472384836389621&display=popup&href=" + current + "&redirect_uri=" + current;
		items.add('share', <a href="+url+" className="Button Button--link">SHARE</a>);*/
	 });
	 extend(HeaderSecondary.prototype, 'items', function(items) {
	 items.add('shop', <a href="t/d" className="Button Button--link">MY</a>);
	 });
	 extend(Post.prototype, 'footerItems', function(items) { 
    console.log(items)
  });
	
	
}); 