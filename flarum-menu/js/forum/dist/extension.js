System.register('binh/menu/main', ['flarum/extend', 'flarum/components/HeaderPrimary', 'flarum/components/HeaderSecondary', 'flarum/components/Post'], function (_export) {
	'use strict';

	var extend, HeaderPrimary, HeaderSecondary, Post;
	return {
		setters: [function (_flarumExtend) {
			extend = _flarumExtend.extend;
		}, function (_flarumComponentsHeaderPrimary) {
			HeaderPrimary = _flarumComponentsHeaderPrimary['default'];
		}, function (_flarumComponentsHeaderSecondary) {
			HeaderSecondary = _flarumComponentsHeaderSecondary['default'];
		}, function (_flarumComponentsPost) {
			Post = _flarumComponentsPost['default'];
		}],
		execute: function () {

			app.initializers.add('binh-menu', function () {

				extend(HeaderPrimary.prototype, 'items', function (items) {
					items.add('shop', m(
						'a',
						{ href: 't/a', className: 'Button Button--link' },
						'SHOP'
					));
					items.add('product', m(
						'a',
						{ href: 't/b', className: 'Button Button--link' },
						'PRODUCT'
					));
					/*var current = top.location.href;
     var url = "https://www.facebook.com/dialog/share?app_id=1472384836389621&display=popup&href=" + current + "&redirect_uri=" + current;
     items.add('share', <a href="+url+" className="Button Button--link">SHARE</a>);*/
				});
				extend(HeaderSecondary.prototype, 'items', function (items) {
					items.add('shop', m(
						'a',
						{ href: 't/d', className: 'Button Button--link' },
						'MY'
					));
				});
				extend(Post.prototype, 'footerItems', function (items) {
					console.log(items);
				});
			});
		}
	};
});