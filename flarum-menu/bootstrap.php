<?php
use Flarum\Event\PostWillBeSaved;
use Flarum\Event\ConfigureClientView;
use Illuminate\Contracts\Events\Dispatcher;

return function (Dispatcher $events) {
	$events->listen(ConfigureClientView::class, function (ConfigureClientView $event) {
    if ($event->isForum()) {
        $event->addAssets(__DIR__.'/js/forum/dist/extension.js');
        $event->addBootstrapper('binh/menu/main');
    }
});
$events->listen(PostWillBeSaved::class, function (PostWillBeSaved $event) {
        // do stuff before a post is saved
        $content = $event->post->content;
				$preg = '<<\S+>>';
				$match = array();
				if(preg_match_all($preg, $content, $match)){
					
					$count = count($match);
					if($count  > 0 ){
						for($i = 0; $i < $count; $i++){
							$link = substr($match[$i][0], 2, strlen($match[$i][0]) - 4);
							$event->post->content = str_ireplace( $match[$i][0], "<iframe href='".$link."' ></iframe>", $content);
						}
					}
				}
    });
};

